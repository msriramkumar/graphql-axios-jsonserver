# graphql-axios-jsonserver

### Make structured RESTful requests using graphql.

### Steps:
1.  Clone repo.
2.  cd into the repo.
3.  run "npm run jserver" & "npm run build"
4.  goto localhost:3000
5.  Make requests calls.
