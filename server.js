const express = require('express');
const expressGraphQL = require('express-graphql');
const schema = require('./schema');
let app = express();


app.use('/',expressGraphQL({
  schema,
  graphiql:true
}))

app.get('/',function (req,res) {

})


app.listen(3000,()=> console.log("Running on port 3000"))
