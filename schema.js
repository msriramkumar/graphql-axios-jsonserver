const axios = require('axios');

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull
} = require('graphql');

//PostsType

const postsType = new GraphQLObjectType({
  name:"posts",
  fields:
    {
    userId:{type:GraphQLInt},
    id:{type:GraphQLInt},
    title:{type:GraphQLString},
    body:{type:GraphQLString}
  }
})

//Root Query

const RootQuery = new GraphQLObjectType({
  name:'RootQueryType',
  fields:{
    post:{
        type:postsType,
        args:{
          id:{type:GraphQLInt}
        },
      resolve(parentValue,args){
          return axios.get(`http://localhost:3001/posts/${args.id}`).then(res =>res.data)
      }
    },
    postList:
    {
      type:new GraphQLList(postsType),
      resolve(parentValue,args)
      {
        return axios.get(`http://localhost:3001/posts/`).then(res=>res.data)
      }
    }

  }

})

module.exports = new GraphQLSchema({
  query:RootQuery
})
